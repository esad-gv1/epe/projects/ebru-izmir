// page drag
const page1 = document.getElementById("paper1");
page1.addEventListener("mousedown", startDrag);
document.addEventListener("mousemove", drag);
document.addEventListener("mouseup", stopDrag);
let isDragging = false;
let mousePressed = false;
let initialTop = page1.getBoundingClientRect().top;
function startDrag(event) {
  isDragging = true;
  mousePressed = true;
  startY = event.clientY;
}
function drag(event) {
  if (mousePressed) {
    page1.style.cursor = "grabbing";
    if (!isDragging) return;
    const deltaY = event.clientY - startY;
    page1.style.top = initialTop + deltaY + 'px';
    if (parseInt(page1.style.top) > -500) {
      screenCanvas(myCanvas);
      clearCanvas();
      isDragging = false;
      page1.style.transition = "1s ease-out";
      page1.style.top = windowHeight + "px";
      setTimeout(function () {
        page1.style.display = "none";
        page1.style.top = initialTop - 100 + "px";
        setTimeout(function () {
          page1.style.display = "flex";
          setTimeout(function () {
            page1.style.top = initialTop + "px";
            setTimeout(function () {
              page1.style.transition = "none";
            }, 1000);
          }, 100);
        }, 100);
      }, 1000);
    }
  } else {
    stopDrag();
  }
}
function stopDrag(event) {
  mousePressed = false;
  if (isDragging) {
    page1.style.transition = "1s ease-out";
    page1.style.top = initialTop + 'px';
    setTimeout(function () {
      page1.style.transition = "none";
    }, 1000);
  }
  page1.style.cursor = "grab";
  isDragging = false;
}

//slider
const sliders = document.querySelectorAll(".slider");
sliders.forEach((slider) => {
  slider.etat = "open"; // Définir la propriété "etat" sur le conteneur
  const button = slider.querySelector(".button");
  button.addEventListener("click", toggleSlider);
});
const closeButton = document.getElementById("closeButton");
closeButton.addEventListener("click", function () {
  sliders.forEach((slider) => {
    goClose(slider);
    slider.etat = "close"
  });
});
function toggleSlider(event) {
  if (event.currentTarget.parentElement.classList.contains("slider")) {
    const slider = event.currentTarget.parentElement; // Utiliser event.currentTarget
    const sliderIndex = Array.from(sliders).indexOf(slider);
    if (slider.etat === "open" && sliderIndex < sliders.length - 1 && sliders[sliderIndex + 1].etat === "open") {
      closeSlidersBelow(sliderIndex);
    } else if (slider.etat === "close" && sliderIndex > 0 && sliders[sliderIndex - 1].etat === "close") {
      goOpen(slider);
      slider.etat = "open";
      openSlidersUpper(sliderIndex);
    } else if (slider.etat === "open") {
      goClose(slider);
      slider.etat = "close";
    } else if (slider.etat === "close") {
      goOpen(slider);
      slider.etat = "open";
    }
  }
}
function closeSlidersBelow(index) {
  for (let i = index + 1; i < sliders.length; i++) {
    const slider = sliders[i];
    goClose(slider);
    slider.etat = "close";
  }
}
function openSlidersUpper(index) {
  for (let i = index - 1; i >= 0; i--) {
    const slider = sliders[i];
    goOpen(slider);
    slider.etat = "open";
  }
}
function goOpen(slider) {
  slider.style.right = "0";
}
function goClose(slider) {
  slider.style.right = `-${slider.offsetWidth}px`;
}

//clear tool
const TOOLclear = document.getElementById("TOOLclear");
TOOLclear.addEventListener("click", function () {
  clearCanvas();
});

// tool selection
const TOOLstickOption = document.getElementById("TOOLstickOption");
const TOOLcombOption = document.getElementById("TOOLcombOption");
const TOOLinkOption = document.getElementById("TOOLinkOption");
const tools = document.querySelectorAll(".toolDiv");
let actualTool = "TOOLstick";
function handleMouseHover(tool) {
  if (tool.etat === "close") {
    tool.style.backgroundColor = "rgb(175, 175, 175)";
  }
}
tools.forEach((tool) => {
  if (tool.id == "TOOLstick") {
    tool.etat = "open";
  } else {
    tool.etat = "close";
  }
  tool.addEventListener("mousedown", (event) => {
    tools.forEach((t) => {
      t.style.backgroundColor = "white";
      t.etat = "close";
      /////////////////////
      TOOLstickOption.style.display = "none";
      TOOLcombOption.style.display = "none";
      TOOLinkOption.style.display = "none";
    });
    if (tool.id == "TOOLstick") {
      TOOLstickOption.style.display = "flex";
    } else if (tool.id == "TOOLcomb") {
      TOOLcombOption.style.display = "flex";
    } else if (tool.id == "TOOLcolor") {
      TOOLinkOption.style.display = "flex";
    }
    tool.style.backgroundColor = "rgb(129, 129, 129)";
    tool.etat = "open";
    actualTool = tool.id;
  });
  tool.addEventListener("mouseenter", () => handleMouseHover(tool));
  tool.addEventListener("mouseleave", () => {
    if (tool.etat === "close") {
      tool.style.backgroundColor = "white";
    }
  });
});

//advenced tool options COLOR
const BGcolorDiv = document.getElementById("BGcolor");
const TXTcolorDiv = document.getElementById("TXTcolor");
const DcolorDiv = document.getElementById("Dcolor");
let BGcolor = '#FFFFFF';
let TXTcolor = '#000000';
let Dcolor = '#000000';
BGcolorDiv.value = BGcolor;
BGcolorDiv.addEventListener("input", function (event) {
  BGcolor = event.target.value;
});
TXTcolorDiv.value = TXTcolor;
TXTcolorDiv.addEventListener("input", function (event) {
  TXTcolor = event.target.value;
});
DcolorDiv.value = Dcolor;
DcolorDiv.addEventListener("input", function (event) {
  Dcolor = event.target.value;
});

//advenced tool options COLOR
const stickSizeSlider = document.getElementById("stickSizeSlider");
const combSizeSlider = document.getElementById("combSizeSlider");
const combNumberSlider = document.getElementById("combNumberSlider");
const combWidthSlider = document.getElementById("combWidthSlider");
const inkDropSizeSlider = document.getElementById("inkDropSizeSlider");
const inkDropNumberSlider = document.getElementById("inkDropNumberSlider");
let stickSize = "100";
let combSize = "100";
let combNumber = "5";
let combWidth = "25";
let inkDropSize = "5";
let inkDropNumber = "1";
stickSizeSlider.value = stickSize;
stickSizeSlider.addEventListener("input", function (event) {
  stickSize = event.target.value;
});
combSizeSlider.value = combSize;
combSizeSlider.addEventListener("input", function (event) {
  combSize = event.target.value;
});
combNumberSlider.value = combNumber;
combNumberSlider.addEventListener("input", function (event) {
  combNumber = event.target.value;
});
combWidthSlider.value = combWidth;
combWidthSlider.addEventListener("input", function (event) {
  combWidth = event.target.value;
});
inkDropSizeSlider.value = inkDropSize;
inkDropSizeSlider.addEventListener("input", function (event) {
  inkDropSize = event.target.value;
});
inkDropNumberSlider.value = inkDropNumber;
inkDropNumberSlider.addEventListener("input", function (event) {
  inkDropNumber = event.target.value;
});

//title from MD
// setTimeout(function () {
//   console.log(mdFilesList);
// }, 1500);

// const response = await fetch(mdFilesList[0]);
// const mdContent = await response.text();
// const tempDiv = document.createElement('div');
// tempDiv.innerHTML = mdContent;
// const divContent = tempDiv.querySelector('#coverTitle').innerHTML;
// const formattedText = divContent.replace(/<br>/g, ' ').trim();

// text selection
let selection = " suyu nakşetmek";
setTimeout(function () {
  drawText(selection, 500, 500)
}, 500);
let actualDrag = false;
let selectionDiv, selectionDivID, Xselection, Yselection;
sliders[2].addEventListener("mouseup", (event) => {
  selection = window.getSelection().toString();
  let limitText = 48;
  if (sliders[2].etat == "open" && selection != "" && selection != " " && selection != undefined) {
    if (selection.length > limitText) {
      selection = selection.substring(0, limitText);
    }
    if (!actualDrag) {
      window.addEventListener("mousemove", dragSelection);
      window.addEventListener("click", putSelection);
      sliders.forEach((slider) => {
        goClose(slider);
        slider.etat = "close"
      });
    }
    if (selectionDivID) {
      selectionDivID.innerText = selection;
    }
  }
})
function dragSelection(event) {
  if (!actualDrag) {
    selectionDiv = createDiv(selection);
    selectionDiv.id("selectionDiv");
    selectionDivID = document.getElementById("selectionDiv");
    actualDrag = true;
  }
  let x = event.clientX - selectionDivID.offsetWidth / 2;
  let y = event.clientY - 30;
  selectionDiv.position(x, y);
  document.body.style.cursor = "grabbing";
}
function putSelection(event) {
  if (targetElement.tagName === "CANVAS") {
    actualDrag = false;
    selectionDiv.remove();
    window.removeEventListener("mousemove", dragSelection);
    window.removeEventListener("click", putSelection);
    document.body.style.cursor = "auto";
    Xselection = event.clientX;
    Yselection = event.clientY;
    drawText(selection, Xselection, Yselection)
  }
}

//screenCanvas picture maker
const imgDivContainer = document.getElementById("imgDivContainer");
let pictureVar = [];
function screenCanvas() {
  pictureVar = [];
  var dataURL = myCanvas.canvas.toDataURL('image/png', 0.9); // Use myCanvas.canvas to reference the actual HTML canvas element
  var blob = dataURLToBlob(dataURL);
  var imageURL = URL.createObjectURL(blob);
  pictureVar.push(imageURL);
  console.log(pictureVar);
  var img = document.createElement('img');
  img.src = imageURL;
  img.className = "screenCanvaspicture";
//   let allImg = document.querySelectorAll(".screenCanvaspicture");
// allImg.forEach((imgWithClass) => {
//     if (imgWithClass.classList.contains("printIMG")) {
//         imgWithClass.classList.remove("printIMG");
//     }
// });
  // img.className = "printIMG";
  imgDivContainer.appendChild(img);


  function dataURLToBlob(dataURL) {
    var parts = dataURL.split(';base64,');
    var contentType = parts[0].split(':')[1];
    var raw = window.atob(parts[1]);
    var rawLength = raw.length;
    var uInt8Array = new Uint8Array(rawLength);
    for (var i = 0; i < rawLength; ++i) {
      uInt8Array[i] = raw.charCodeAt(i);
    }
    return new Blob([uInt8Array], { type: contentType });
  }
}

//picture to print
// const imagePrint = document.querySelectorAll(".Image1");
// function changeSrcPicturePrint() {
//   for (let i = 0; i < array.length; i++) {
//     const element = array[i];
    
//   }
//   imagePrint.forEach(image => {
//     image.src = 
//   });
// }