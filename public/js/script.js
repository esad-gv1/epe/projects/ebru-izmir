const sliders = document.querySelectorAll(".slider");
const tools = document.querySelectorAll(".toolDiv");

sliders.forEach((slider) => {
  slider.etat = "open"; // Définir la propriété "etat" sur le conteneur
  const button = slider.querySelector(".button");
  button.addEventListener("mousedown", toggleSlider);
});

function toggleSlider(event) {
  const slider = event.currentTarget.parentElement; // Utiliser event.currentTarget
  const sliderIndex = Array.from(sliders).indexOf(slider);

  if (slider.etat === "open" && sliderIndex < sliders.length - 1 && sliders[sliderIndex + 1].etat === "open") {
    closeSlidersBelow(sliderIndex);
  } else if (slider.etat === "close" && sliderIndex > 0 && sliders[sliderIndex - 1].etat === "close") {
    goOpen(slider);
    slider.etat = "open";
    openSlidersUpper(sliderIndex);
  } else if (slider.etat === "open") {
    goClose(slider);
    slider.etat = "close";
  } else if (slider.etat === "close") {
    goOpen(slider);
    slider.etat = "open";
  }
}

function closeSlidersBelow(index) {
  for (let i = index + 1; i < sliders.length; i++) {
    const slider = sliders[i];
    goClose(slider);
    slider.etat = "close";
  }
}

function openSlidersUpper(index) {
  for (let i = index - 1; i >= 0; i--) {
    const slider = sliders[i];
    goOpen(slider);
    slider.etat = "open";
  }
}

function goOpen(slider) {
  slider.style.right = "0";
}

function goClose(slider) {
  slider.style.right = `-${slider.offsetWidth}px`;
}
// tool selection
let actualTool = "TOOLstick";
function handleMouseHover(tool) {
  if (tool.etat === "close") {
    tool.style.backgroundColor = "rgb(175, 175, 175)";
  }
}
tools.forEach((tool) => {
  if (tool.id == "TOOLstick") {
    tool.etat = "open";
  } else {
  tool.etat = "close";
}
  tool.addEventListener("mousedown", (event) => {
    tools.forEach((t) => {
      t.style.backgroundColor = "white";
      t.etat = "close";
    });
    tool.style.backgroundColor = "rgb(129, 129, 129)";
    tool.etat = "open";
    actualTool = tool.id;
  });
  tool.addEventListener("mouseenter", () => handleMouseHover(tool));
  tool.addEventListener("mouseleave", () => {
    if (tool.etat === "close") {
      tool.style.backgroundColor = "white";
    }
  });
});
// text selection
sliders[2].addEventListener("mouseup", (event) => {
  const selection = window.getSelection().toString();
  if (sliders[2].etat == "open" && selection != "" && selection != " " && selection != undefined) {
    console.log(selection.length);
  drawText(selection)}
})

let tempToDraw = "hello world";