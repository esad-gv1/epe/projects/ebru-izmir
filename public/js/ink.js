
class Drop {
  constructor(x, y, r, col, letter) { // changed
    this.center = createVector(x, y);
    this.r = r;
    let letterToDraw = letter;//String.fromCharCode(65 + floor(random(26)));
    // let letterToDraw = textToDraw.toUpperCase();
    this.points = font.textToPoints(letterToDraw, x, y, this.r
      //random(100, 800), random(200, 600), random(30, 200)
      , { sampleFactor: 1 });
    this.vertices = [];
    for (let point of this.points) {
      let v = createVector(point.x, point.y);
      this.vertices.push(v);
    }
    this.col = col;

    let minX = Infinity;
    let maxX = -Infinity;
    let minY = Infinity;
    let maxY = -Infinity;
    for (let v of this.vertices) {
      minX = min(minX, v.x);
      maxX = max(maxX, v.x);
      minY = min(minY, v.y);
      maxY = max(maxY, v.y);
    }
    this.rect = {
      left: minX,
      right: maxX,
      top: minY,
      bottom: maxY
    };
  }

  tine(m, x, y, z, c) {
    let u = 1 / pow(2, 1 / c);
    let b = createVector(x, y);
    for (let v of this.vertices) {
      let pb = p5.Vector.sub(v, b);
      let n = m.copy().rotate(HALF_PI);
      let d = abs(pb.dot(n));
      let mag = z * pow(u, d);
      v.add(m.copy().mult(mag));
    }
  }

  marble(other) {
    for (let v of this.vertices) {
      let c = other.center;
      let r = other.r/2;
      let p = v.copy();
      p.sub(c);
      let m = p.mag();
      let root = sqrt(1 + (r * r) / (m * m));
      p.mult(root);
      p.add(c);
      v.set(p);
    }
  }

  show() {
    // fill(this.col);
    fill(TXTcolor);
    noStroke();
    beginShape();
    for (let v of this.vertices) {
      vertex(v.x, v.y);
    }
    endShape(CLOSE);
  }
}

class DropInk {
  constructor(x, y, r, col) {
    this.center = createVector(x, y);
    this.r = r;
    this.circleDetail = this.r * 16;
    this.vertices = [];
    for (let i = 0; i < this.circleDetail; i++) {
      let angle = map(i, 0, this.circleDetail, 0, TWO_PI);
      let v = createVector(cos(angle), sin(angle));
      v.mult(this.r);
      v.add(this.center);
      this.vertices[i] = v;
    }
    this.col = col;
  }


  tine(m, x, y, z, c) {
    let u = 1 / pow(2, 1 / c);
    let b = createVector(x, y);
    for (let v of this.vertices) {
      let pb = p5.Vector.sub(v, b);
      let n = m.copy().rotate(HALF_PI);
      let d = abs(pb.dot(n));
      let mag = z * pow(u, d);
      v.add(m.copy().mult(mag));
    }
  }

  marble(other) {
    for (let v of this.vertices) {
      let c = other.center;
      let r = other.r;
      let p = v.copy();
      p.sub(c);
      let m = p.mag();
      let root = sqrt(1 + (r * r) / (m * m));
      p.mult(root);
      p.add(c);
      v.set(p);
    }
  }

  show() {
    // fill(this.col);
    fill(Dcolor);
    noStroke();
    beginShape();
    for (let v of this.vertices) {
      vertex(v.x, v.y);
    }
    endShape(CLOSE);
  }
}
