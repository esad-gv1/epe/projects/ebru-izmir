import { loadScripts } from "./js/utils.js";
import { paginate } from "./js/paginate.js";

const elementsToPaginate = [];
const contentDiv = document.getElementById("content");

//CSS used in this project, including the pagedjs preview css
const styleList = [
    "css/style.css",
    "css/cover.css",
    "css/back.css",
    "vendors/css/paged-preview.css"
];

//additional script to load, not importable as es modules
const scritpList = [
    "vendors/js/markdown-it.js",
    "vendors/js/markdown-it-footnote.js",
    "vendors/js/markdown-it-attrs.js",
    "vendors/js/markdown-it-container.js",
    "vendors/js/markdown-it-span.js",
];

//sync batch loading
await loadScripts(scritpList);

//markdown files to load
const mdFilesList = ["md/cover.md", "md/main.md", "md/back.md"];
//html elements to be filled from converted md file
const partsList = ["cover", "textDiv", "back"];

//markdownit instanciation (old school method as no ES6 modules are available)
const markdownit = window.markdownit
    ({
        // Options for markdownit
        langPrefix: 'language-fr',
        // You can use html markup element
        html: true,
        typographer: true,
        // Replace english quotation by french quotation
        quotes: ['«\xA0', '\xA0»', '‹\xA0', '\xA0›'],
    })
    .use(markdownitContainer) //div
    // .use(markdownitSpan) //span
    .use(markdownItAttrs, { //custom html element attributes
        // optional, these are default options
        leftDelimiter: '{',
        rightDelimiter: '}',
        allowedAttributes: [] // empty array = all attributes are allowed
    });

async function layoutHTML() {

    //!important! forEach can't be used as it doesn't respect await order!
    for (let index = 0; index < mdFilesList.length; index++) {

        const mdFile = mdFilesList[index];
        const reponse = await fetch(mdFile);
        const mdContent = await reponse.text();
        //convertion from md to html, returns a string
        const result = markdownit.render(mdContent);

        const destinationElement = document.getElementById(partsList[index]);
        destinationElement.innerHTML = result;

        elementsToPaginate.push(destinationElement.cloneNode(true));
    };

}

window.addEventListener("load", async (event) => {
    await layoutHTML();
});

document.getElementById("printBt").addEventListener("click", () => {
    const screenStylesheet = document.getElementById("screenCss");
    screenStylesheet.parentNode.removeChild(screenStylesheet);

    //document.getElementById("mainCss").parentNode.removeChild(mainStylesheet);
    //elementsToPaginate.push(contentDiv.cloneNode(true));
    //removeScrollListener();
    // console.log(styleList)

    setTimeout(function () {
        let image1 = document.querySelectorAll("img");
        for (let i = 0; i < image1.length; i++) {
            if (pictureVar[0]) {
                image1[i].src = pictureVar[0];
            }
            let parentHeight = image1[i].parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.getBoundingClientRect().height;
            let parentWidth = image1[i].parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.getBoundingClientRect().width;
            let imageWidth = image1[i].getBoundingClientRect().width;
            let leftPosition = parentWidth - (imageWidth / 6);
            image1[i].style.height = parentHeight + "px";
            image1[i].style.top = 0 + "px";
            if (i == 0) {
                image1[i].style.left = -leftPosition + "px";
                console.log(parentWidth);
            } else if (i == 1) {
                image1[i].style.right = -leftPosition + "px";
                console.log(imageWidth);
            }
        }
    }, 500);


    paginate(elementsToPaginate, styleList);
    document.body.style.overflow = "auto";

});

