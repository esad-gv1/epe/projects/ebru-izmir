<div class="türkçe">
“Ebru sanatının diğer geleneksel sanatlara göre en büyük farkı nedir?” diye sorulduğunda, verilecek ilk
cevap ebru sanatının tekrarı olmayan bir sanat olması hasebiyle diğer sanat dallarına göre farklı bir yapıya sahip olduğudur. Bu kanıyı geliştirecek olursak ebru sanatının tıpkı bir yaşam gibi tekrarı yoktur; benzersiz, tek ve biricik. Çünkü her ebru kâğıdı, benzerlik açısından tek yumurta ikizi gibi algılansa da aslında her ebrunun kendi karakteristiğinde yapılmıştır ve gelecekte yapılacak diğer tüm ebrulardan daima farklı olacaktır. 
</div>

<div class="english">
When asked, "What is the biggest difference between the art of marbling (ebru) and other traditional arts?" the first answer that comes to mind is that the art of marbling is unique in that it cannot be repeated, which gives it a different nature compared to other art forms. To elaborate on this notion, the art of marbling, much like life itself, has no repetition; it is unique, singular, and one-of-a-kind. Even though each marbled paper might be perceived as similar to identical twins, in reality, every marbling piece is created with its own characteristics and will always be different from all other marblings made in the past or in the future.
</div>

<!-- Visual Computing Institute, RWTH Aachen University{.bottomtext} -->