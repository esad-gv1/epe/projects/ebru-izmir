<div class="specialsize3 content">

Ebru sanatının tıpkı parmak izimiz, retinamız, yüzümüz, hatta kalp atımlarımız ve sesimiz gibi farklı, kendine özgüdür. Tekrarı olmayan yaşamlarımız gibi.<br>

Each marbled paper is unique and distinct, much like our fingerprints, retinas, faces, even our heartbeats and voices. Just like our lives that have no repetition. 

</div>





<div class="türkçe1">

# Ebru Sanatı ve Tarihçesi {.breakClassA}

Eski kitapların kapaklarında ve yan kâğıtlarında rastladığımız ebru, yoğunlaştırılmış su üzerine toprak ve toz boyalarla resim yapma sanatıdır.  Kitre  veya  benzeri  maddelerle yoğunluğu  arttırılmış  su  üzerine,  özel  fırçalar  yardımıyla  boyaların  serpilerek,  su  yüzeyinde meydana gelen desenlerin kâğıda geçirilmesiyle elde edilir.Usta-çırak yöntemi ile öğrenilen ve sanatçının iradesi dışında birçok değişkenden etkilenen bir  sanattır.  Bazı  kaynaklar  ebrunun,  yüzsuyu  anlamına  gelen  "ab-ıru"  sözcüğünden,  bazı kaynaklar ise Orta Asya dillerinden Çağatayca’da hareli görünüm, damarlı kumaş ya da kâğıt anlamına  gelen  "ebre"  den  geldiğini  söylese  de  en  yaygın  kanı,  kelimenin  kökeninin  Farsça; bulutumsu, bulut gibi anlamına gelen "ebri" den gelmekte olduğudur. 
</div>

<div class="english1">

# History of Ebru Art 

Marbling (ebru), found on the covers and endpapers of old books, is the art of painting with earth and powdered dyes on thickened water. It is created by sprinkling paints onto water thickened with tragacanth or similar substances using special brushes, then transferring the patterns formed on the water's surface onto paper. This art is learned through the master-apprentice method and is influenced by many variables beyond the artist's control. While some sources claim that the term "ebru" derives from the word "ab-ıru," meaning "water face," and others suggest it comes from the Chagatai word "ebre," meaning "veined appearance" or "veined fabric or paper," the most widely accepted belief is that the term originates from the Persian word "ebri," meaning "cloudy" or "like a cloud."
</div>

<div class="artPara">

The art of marbling (ebru), widely used during the Seljuk and Ottoman periods, was introduced to Europe at the beginning of the 17th century under the name "Turkish Paper." Initially, this traditional art captivated European aristocracy and was always associated with the word "Turkish." During the same period, some diplomats, travelers, and religious and scientific figures, who felt the intense influence of the Ottoman Empire in every field, tried to introduce their societies to the Ottomans through their writings about what they had seen in the Ottoman country. In 1592, Viennese aristocrat Anna Maria van Heusenstain referred to "those called Turkish papers..." while describing veined tulips. French aristocrat Pierre De L’Estoile gifted his friends "marbled papers" and notebooks "covered with marbled papers." Marbling had started to attract the interest of foreign travelers. At this time, marbled papers were considered products of refined taste among the European upper class and were in high demand. With the recognition of marbling in the West, its production also began and became increasingly widespread in the first half of the 17th century.{.breakClass}

</div>

<div class="artPara2">

Selçuklular ve Osmanlılar döneminde yaygın olarak kullanılan ebru sanatı, XVII. Yüzyılın başlarında Türk Kağıdı adı ile Avrupa’ya açıldı. Avrupa’da aristokrasinin ilgisini çekmeye başlayan geleneksel sanatımız Ebru, başlangıçta hep Türk kelimesiyle beraber anılırdı. Aynı dönemde, bazı diplomatlar,  seyyahlar,  din  ve  bilim  adamları;  her  alanda  etkilerini  yoğun  olarak  hissettikleri Osmanlı  ülkesinde  gördüklerini  anlatan  yazılarıyla  Osmanlı’ya  toplumlarını  tanıtmaya çalışmışlardı. 1592’de, Viyanalı Aristokrat Anna Maria van Heusenstain, hareli laleleri anlatırken “Türk  kâğıdı  denilenlerden...”  diye  yazıyordu.  Fransız  Aristokratı  Pierre  De  L’Estoile  ise, dostlarına “ebrulu kâğıtlar” ve  “ebrulu kâğıtlarla  kaplı” defterler hediye ediyordu. Ebru artık yabancı  seyyahların  ilgisini  çekmeye  başlamıştı.  Bu  sırada  ebrulu  kâğıtlar  Avrupa’nın  üst tabakasında ince zevk ürünü olarak kabul edilip rağbet görmekteydi. Ebrunun batıda tanınmasıyla birlikte üretimi de başlamış ve 17. Yüzyılın ilk yarısında giderek yaygınlaşmıştır.{.breakClass}

</div>




### Ebru Sanatı ve Terapi{.breakClass}

<div class="specialsize2">
Sanatın tedavi amaçlı kullanılması; hastalar için duygusal çatışmaları onarmak, kişisel gelişimi arttırmak, sözel olarak ifade edemedikleri ve hastalıkları hakkındaki kaygılarını ifade etmek için bir yoldur. Edebiyat, sanat ve müzik bağlamında, şairler, yazarlar sözcükleri, ressamlar ve ebru ustaları renkleri; bestekar ve müzisyenler ise sesleri derler. Hayatın akışı içinde uyumsuzluk görüntüsü veren durumlarda insan, özündeki uyumu açığa çıkaran durumlarda  rahatlar.  Renklerin  musikisi olarak nitelediğimiz ebru’da ruhu rahatlatan, ruhu dinlendiren bir uyum vardır. Ebru boyalarının su yüzeyindeki sonsuz renk dansı, belli bir armoni ve uyumu gerektirir. Ebru, görsel zerafetin yanı sıra, bizlere  mikro ve  makroalemlerden, çıplak gözün göremeyeceği ilginç  güzellikler sunar. Ayrıca Ebru’nun terapi özelliğine  sahip olduğu, tartışılmayan bir gerçektir. Geçmiş zamanlarda, İslam ülkelerindeki hastaneler ve tımarhanelerde ruh hastaları musiki ve güzel sanatlarla tedavi edilirdi.{.breakClass}
</div>



<div class="specialsize4">
Ebru sanatı öncelikle sabırlı olmayı öğretip, bu meziyetin geliştirilmesine katkı sağlar.  Ebru sanatçısı olan birey her daim kendine tam hakim değildir, özellikle obsesif bozukluğu olan kişiler, var olanı kabullenmeyi zaten öğrenirler.  Ebru  sanatının terapötik açıdan olumlu sonuçlar verdiği deneyimler ile kanıtlanmış olup, estetik hassasiyet, iletişim kurmak, zamanı doğru kullanmak,  motivasyon,  yaratıcılık,  sabırlı  olmak,  disiplinli  olmak,  intibak  sağlayabilme hususlarının iyileştirilmesine katkı sağlayarak aynı zamanda bu sanat sayesinde stres, dengesizlik ve anksiyete gibi belirli sorunların aşılması konularında olumlu sonuçlar alınmıştır. Ebru sanatı öncelikle kabullenmeyi öğretir. Ebru sanatında, şekiller sanatı icra eden kişinin  tamamen iradesi  dışında  gelişirler,  kişi  suya  düşen  damlaları,  damlaların  kümelenmelerini  ve  aralarında oluşan  boşlukları  kontrol  edemez.  Böylelikle,  tam  hakimiyet  sağlayamadığı  için  obsesif bozukluklarda zaman içinde azalma kaydedilir.

Ebru sanatı kişinin rutin haline gelmiş yaşamına bir soluk vererek onu farklı dünyalara götürür ve ruhuna sakinlik aşılar.  Renkler ve şekiller bireye anlık  duygularını  ifade  edebilme  olanağını  sağlar.  Ebru  sanatının  diğer  faydaları  şöyle  ifade edilebilir: kişi stres ile baş edebilmeyi öğrendiğinde ve motivasyon kazandığında yaratıcı gücü ve kendine güveni artmaktadır.  Suyun  insanlar üzerinde  olumlu  etkisi  vardır,  özellikle  su  pozitif  iyonlar  yaydığı  zaman.Çocuk dikkatini  suya  ve  hareket  halindeki  renklere  odakladığında  gerginliği  azalır  ve  zamanın  nasıl geçtiğini anlayamaz.  Buna  ilaveten,  çocuk  şekillerle  bir  sanat eseri  yaratır, bu  eser  sayesinde başkalarının takdirini kazandığında kendine olan güveni de artmış olur.
</div>

##### Ebru Art and Therapy{.breakClass}
<div class="specialsize6 columnClass">
The art of marbling (ebru) primarily teaches patience and contributes to the development of this virtue. Individuals practicing marbling are not always in full control, especially those with obsessive disorders, who learn to accept what exists. The therapeutic benefits of marbling have been proven through experiences, contributing to improvements in aesthetic sensitivity, communication, proper time management, motivation, creativity, patience, discipline, and adaptability. Additionally, positive results have been achieved in overcoming specific issues such as stress, imbalance, and anxiety.

Marbling teaches acceptance. In marbling, the patterns develop entirely outside the artist's control. The person cannot control the drops falling on the water, the clustering of the drops, or the spaces forming between them. Thus, since complete control is not possible, there is a reduction in obsessive disorders over time. Marbling provides a break from the individual's routine life, taking them to different worlds and instilling calmness in their soul. Colors and shapes offer the individual the opportunity to express their instant emotions.

Other benefits of marbling include: when a person learns to cope with stress and gains motivation, their creative power and self-confidence increase. Water has a positive effect on people, especially when it emits positive ions. When a child focuses on the water and the moving colors, their tension decreases, and they lose track of time. Additionally, when a child creates an artwork with shapes and gains the appreciation of others, their self-confidence increases.
</div>

######

######
![](){.Image1}
######
![](){.Image1}
######

######




#### Abstract{.breakClass}
<div class="specialsize7">
Geleneksel el sanatları, toplumların duygularını, düşüncelerini, kültürlerini ve kimliklerini belgelendiren önemli unsurlardır. Bu sanatlar, yüzyıllar boyunca bir nehir misali akarak, toplumlara geçmişin bilgi hafızasını ve duygu birikimlerini taşımıştır. Uluslar, bu kültürel nehirden beslenerek farklı kültürel özellikler kazanmışlardır.


Bu geleneksel sanatlar arasında en önemli olanlardan biri ebru sanatıdır. Ebru sanatı, yüzyılların sözlü ve pratik uygulamalarıyla geleneksel usta-çırak yöntemiyle aktarılmaktadır. UNESCO tarafından koruma altına alınması, Türk toplumu ve dünya milletleri arasında bu sanata olan ilgi ve farkındalığı artırmıştır.UNESCO'nun desteğiyle bu sanat, usta-çırak yönteminin yanı sıra farklı mecralarda da öğretilmeye başlanmıştır.


Ebru sanatı, su üzerinde icra edilişi, eserlerin hızlıca ortaya çıkması ve pozitif psikolojik etkileri ile diğer sanat dallarından ayrılmaktadır. Usta-çırak yöntemi ve ustaya hürmet üzerine kurulu öğrenim süreci, ebru sanatını saygın ve evrensel bir sanat yapma yolunda ilerletmiştir.
</div>

<div class="specialsize8">
Traditional handicrafts are important elements that document the emotions, thoughts, cultures, and identities of societies. Like a river that has flowed for centuries, these arts have carried the collective memory and emotional heritage of the past to communities. Nations have gained distinct cultural features by nourishing from this cultural river.

Among these traditional arts, one of the most significant is the art of marbling, or ebru. Ebru art is transferred through the traditional master-apprentice method, encompassing centuries of oral and practical applications. Its inclusion under UNESCO's protection has increased interest and awareness of this art among both Turkish society and the global community. With UNESCO's support, ebru has begun to be taught in various new ways beyond the traditional master-apprentice method.

Ebru art distinguishes itself from other art forms through its execution on water, the rapid emergence of its creations, and its positive psychological impacts. The learning process, based on the master-apprentice method and respect for the master, has advanced ebru art toward becoming a respected, popular, and universal art form.
</div>



#### References{.breakClass}
<div class="specialsize9">
Meltem Kürtüncü, Latife Utaş Akhan, Sevecen Çelik Atilla, CAN
</div>

<div class="specialsize">
Kürtüncü, M., Utaş Akhan, L.,  & Çelik, S.(2014). Geleneksel Türk Ebru Sanatının kronik hastalığı olan çocukların terapisi üzerine etkisi. International Journal of Human Sciences, 11(2), 598-608.<br>
Aktay, A. (2014). Sanat Terapisi.  http://www.aydaaktay.com/ebru-ve-terapi.asp,E.T. 15.05.2014.<br>
Alp, A.R. (1958). Büyük Osmanlı Lügati. İstanbul: Ekincigil Matbaası.<br>
Aral, N., Kayabaşı, N., Yıldız Bıçakcı, M., Aydın, A., Mutlu, B. (2012). Study of effect of Ebru training on the developmental areas of children aged under five. Social and Behavioral Sciences51,296-300. http://dx.doi.org/10.1016/j.sbspro.2012.08.162<br>
Arıtan, A.S. (1999). Türk Ebru Sanatı ve  Bugünkü Durumu. Selçuk Üniversitesi Sosyal Bilimler Enstitüsü Dergisi5, 441-469.<br> 
Atalay, B. (1970). Abuşka Lügati veya Çağatay Sözlüğü. Ankara: Ayyıldız Matbaası.<br>
Aydın,  B.  (2012). Tıbbi  Sanat  Terapisi.Psikiyatride  Güncel  Yaklaşımlar,4(1),69-83. doi:10.5455/cap.20120405.<br>
Barutçugil, H. (2010). Türklerin Ebru Sanatı. Kültür Bakanluı Yayınları, Ankara.<br>
Beebe,  A., Gelfand,  E.W., Bender,  B.(2010).  A  Randomize  Trial  to  Test  Effectiveness  of  Art Therapy  for  Children  with  Astma. Journal  of  Allergy  Clinical  Immunology,  126(2),  263-6.  doi: 10.1016/j.jaci.2010.03.019.<br>
Diehl, E. (1980). Bookbinding: Its Background and Technique. Newyork: Dover Publications.<br>
Edwards, D. (2004). Art Therapy. London: Sage Publications.<br>
Er, M. (2006). Çocuk, Hastalık, Anne-Babalar ve Kardeş.Çocuk Sağlığı ve Hastalıkları Dergisi,  49, 155-168.<br>
Erdoğan, A.,  Karaman,  M.G.  (2008). Kronik ve Ölümcül Hastalığı Olan Çocuk ve Ergenlerde Ruhsal Sorunların Tanınması ve Yönetilmesi. Anadolu Psikiyatri Dergisi, 9, 244-252.<br>
Favara-Scacco,  C., Smirne,  G., Schilirò, G., Di  Cataldo,  A.(2001).  Art  therapy  as  support  for children with leukemia during painfulprocedures. Med Pediatr Oncol., 36 (4), 474-480.<br>
Fettahoğlu, E.Ç., Koparan, C., Özatalay, E., Türkkahraman, D. (2007). İnsüline Bağımlı Diabetes Mellitus Tanılı Çocuk ve Ergenlerde Gözlenen Ruhsal Güçlükler. Psychiatry in Turkey,9, 32-36.<br>
Findling JH. (2004). Development of a Trauma Play Scale: an Observatıon-Based Assessment of The  Impact  of  Trauma  on  the  Play  Therapy  Behaviors  of  Young  Children.  University  of North Texas. Yayınlanmamış Doktora Tezi. United States.<br>
Foucault, M. (1992). Deliliğin Tarihi. Ankara: İmge Kitabevi.<br>
Gavin,  L.,  Wysocki,  T.  (2006).  Associations  of  Paternal  Involvement  in  Disease  Management with Maternal and Family Outcomes in Families with Children with Chronic Illness. Journal of Pediatric Psychology, 31 (5), 481-489.<br>
GökçekD, Çakır A, Koç S. (2013). Hastane palyaçolarının pediatri hemşireliğine Katkıları. 4. Ulusal Pediatri Hemşireliği Kongresi. Kongre Kitabı, Sözel Bildiri (20), Adıyaman, 54<br>
Gür,  Ç.  (2012).  Turkish  marblıng  and  gifted  children. İnternational  Journal  of  Learning  and Development, 2 (6), 86-92. http://dx.doi.org/10.5296/ijld.v2i6.2555.<br>
İçel,   B.   (2014).   Renklerin   Su   Üstündeki   Kutsal   Dansı:   Ebru   Sanatı. http://bahadiricel.wordpress.com/2009/01/03/renklerin-su-ustundeki-kutsal-dansi-ebru-sanati, E.T. 15.05.2014.<br>
İşler,  A.Ş.  (2003).  Okul  Öncesinde  Disiplin  Temelli  Sanat  Eğitiminin  UygulanabilirliğininKuramsal Temelleri ve Çocuk Gelişimi Açısından Önemi. Atatürk Üniversitesi Kazım Karabekir Eğitim Fakültesi Dergisi, 8, 35-54.<br>

</div>

