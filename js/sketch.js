let drops = [];

let r = 1;
let a = 0;
let myCanvas;

let black; // changed

function preload() { // new
  font = loadFont('../css/fonts/Lineal-Heavy.ttf');
}

function setup() {
  myCanvas = createCanvas(windowWidth, windowHeight);
  myCanvas.id("myCanvas");
  // changed
  black = TXTcolor;
  /*
    for (let i = 0; i < 5; i++) {
      let x = random(width);
      let y = random(height);
      let r = random(100, 200);
      addTextInk(x, y, r, black);
    }*/
  let tempToDraw = "hello world";
  drawText(tempToDraw);
}
function clearCanvas() {
  drops = [];
  background(255, 255, 255);
}

function drawText(textToDraw, x, y) {
  x = x - 600;
  for (let i = 0; i < textToDraw.length; i++) {
    let r = 10 + (1000 - 10) / Math.max(textToDraw.length, 1);
    x += (300 - Math.sqrt(textToDraw.length) * 40);
    addTextInk(x, y, r, black, textToDraw[i]);
  }
}

function tineLineStick(v, x, y, z, c) { // changed
  for (let drop of drops) {
    for (let vertex of drop.vertices) {
      let dx = abs(x - vertex.x);
      let dy = abs(y - vertex.y);
      if (dx < stickSize && dy < stickSize) {
        drop.tine(v, x, y, z / 80, c);
      }
    }
  }
}

let brushHeight = -(2 * (combWidth / 2) - 68);
function tineLineComb(v, x, y, z, c) { // changed
  brushHeight = -(2 * (selection.length / 2) - 68);
  for (let drop of drops) {
    for (let vertex of drop.vertices) {
      let dx = abs(x - vertex.x);
      let dy = abs(y - vertex.y);
      if (dx < combSize && dy < combSize) {
        for (let i = 0; i < combNumber; i++) {
          drop.tine(v, x - (i * brushHeight), y - (i * brushHeight), z / 40, c);
          drop.tine(v, x + (i * brushHeight), y + (i * brushHeight), z / 40, c);
        }
      }
    }
  }
}

function addTextInk(x, y, r, black, letter) {
  let drop = new Drop(x, y, r, black, letter);
  for (let other of drops) {
    other.marble(drop);
  }
  drops.push(drop);
}

function addInk(x, y, r, black) {
  let drop = new DropInk(x, y, r, black);
  for (let other of drops) {
    other.marble(drop);
  }
  drops.push(drop);
}

let val = 4;
var inkDropAdded = false;
function draw() {
  if (mouseIsPressed && targetElement.tagName === "CANVAS") {
    let v1 = createVector(pmouseX, pmouseY);
    let v2 = createVector(mouseX, mouseY);
    v2.sub(v1);
    if (actualTool == "TOOLcolor" && !inkDropAdded && !actualDrag) {
      let r = parseInt(random(inkDropSize, inkDropSize * 5));
      for (let i = 0; i < inkDropNumber; i++) {
        let x = parseInt(random(mouseX / 1.2, mouseX * 1.2));
        let y = parseInt(random(mouseY / 1.2, mouseY * 1.2));
        addInk(x, y, r, black);
      }
      inkDropAdded = true;
    }
    if (v2.mag() > 0.1) {
      v2.normalize();
      if (actualTool == "TOOLstick") {
        tineLineStick(v2, mouseX, mouseY, 2, 16);
      } else if (actualTool == "TOOLcomb") {
        tineLineComb(v2, mouseX, mouseY, 2, 16);
      } //else 
    }
  } else if (actualTool == "TOOLcolor") {
    inkDropAdded = false;
  }
  background(BGcolor);
  for (let drop of drops) {
    drop.show();
  }
}

let targetElement = null;
document.addEventListener("mousedown", function (event) {
  targetElement = event.target;
});
