# EPE

## Fluid Sketching

Content source : *Fluid Sketching: Bringing Ebru Art into VR*, Eroglu, Sevinc; Weyers, Benjamin; Kuhlen, Torsten, Mensch und Computer 2018 - Workshopband. DOI: 10.18420/muc2018-demo-0511. Bonn: Gesellschaft für Informatik e.V.. Interaktive Demos. Dresden. 2.-5. September 2018

## Authors
Simon Tregouët & Zeynep Saçkan

# General concept

Turkish traditional Ebru art, also known as paper marbling, is a decorative technique that involves creating intricate, colorful patterns on the surface of water, which are then transferred to paper or fabric. This ancient art form uses natural pigments, ox gall, and a special tray to float and manipulate the colors before capturing the design. Ebru is characterized by its fluid, swirling designs and has been used historically for bookbinding, calligraphy, and decorative purposes.
This project propose to elaborate upon the text of a group of computer scientist that gave an atempt to bring ebru art in virtual reality space.
The article text is selectable by the user and can be deposit on a kind of liquid screen, here a canvas that uses P5.js. Several tools that the user can choose simulates the liquid surface manipulation that produce de marbling effect in the tradition art, but are used here to distort and twist the rendered text.
Once the visual composition is done, the user can "print" it to insert it in the printed version of the article, layouted with Paged.js.